# Description
Simple calculator app created with Ionic Framework.

#Running
http://ionicframework.com/getting-started/

osx:
brew install npm 
npm install -g cordova ionic
ionic serve
ionic platform add android

export ANDROID_HOME=/Users/dg/Library/Android/sdk/
echo $ANDROID_HOME
export PATH="$ANDROID_HOME/platform-tools:$ANDROID_HOME/tools:$PATH"

sudo ionic cordova build --release android

ln -s /Users/dg/_android/calc/platforms/android/build/outputs/apk/ output

keytool -genkey -v -keystore my-release-key.keystore -alias dg -keyalg RSA -keysize 2048 -validity 10000

sudo jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore output/calcApp.apk dg

sudo /Users/dg/Library/Android/sdk/build-tools/25.0.2/zipalign -v 4 output/calcApp.apk output/calcApp1.apk 


